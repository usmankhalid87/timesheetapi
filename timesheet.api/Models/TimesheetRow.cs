﻿namespace timesheet.api.Models
{
    public class TimesheetRow
    {
        public int TaskId { get; set; }

        public double? SundayEffort { get; set; }

        public double? MondayEffort { get; set; }

        public double? TuesdayEffort { get; set; }

        public double? WednesdayEffort { get; set; }

        public double? ThursdayEffort { get; set; }

        public double? FridayEffort { get; set; }

        public double? SaturdayEffort { get; set; }
    }
}
