﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace timesheet.api.Models
{
    public class EmployeeEffortItem
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public double? CurrentWeekEffort { get; set; }

        public double? AverageWeeklyEffort { get; set; }
    }
}
