﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using timesheet.model;

namespace timesheet.api.Models
{
    public class TimesheetModel
    {
        public TimesheetModel()
        {
            TimesheetRecords = new List<TimesheetRow>();
        }

        public int EmployeeId { get; set; }

        public int Week { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public List<TimesheetRow> TimesheetRecords { get; set; }

        public void MapToTimesheetModel(IEnumerable<TimeSheetRecord> timesheetRecords)
        {
            var taskIds = timesheetRecords.Select(x => x.TaskId).Distinct().ToList();
            DateTime recordDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            foreach (var taskId in taskIds)
            {
                var record = new TimesheetRow();
                record.TaskId = taskId;

                var ts = timesheetRecords.Where(x => x.TimesheetDate == recordDate && x.TaskId == taskId).SingleOrDefault();

                if (ts != null)
                    record.SundayEffort = ts.HoursWorked;

                recordDate = recordDate.AddDays(1);
                ts = timesheetRecords.Where(x => x.TimesheetDate == recordDate && x.TaskId == taskId).SingleOrDefault();

                if (ts != null)
                    record.MondayEffort = ts.HoursWorked;

                recordDate = recordDate.AddDays(1);
                ts = timesheetRecords.Where(x => x.TimesheetDate == recordDate && x.TaskId == taskId).SingleOrDefault();

                if (ts != null)
                    record.TuesdayEffort = ts.HoursWorked;                

                recordDate = recordDate.AddDays(1);
                ts = timesheetRecords.Where(x => x.TimesheetDate == recordDate && x.TaskId == taskId).SingleOrDefault();

                if (ts != null)
                    record.WednesdayEffort = ts.HoursWorked;

                recordDate = recordDate.AddDays(1);
                ts = timesheetRecords.Where(x => x.TimesheetDate == recordDate && x.TaskId == taskId).SingleOrDefault();

                if (ts != null)
                    record.ThursdayEffort = ts.HoursWorked;

                recordDate = recordDate.AddDays(1);
                ts = timesheetRecords.Where(x => x.TimesheetDate == recordDate && x.TaskId == taskId).SingleOrDefault();

                if (ts != null)                
                    record.FridayEffort = ts.HoursWorked;

                recordDate = recordDate.AddDays(1);
                ts = timesheetRecords.Where(x => x.TimesheetDate == recordDate && x.TaskId == taskId).SingleOrDefault();

                if (ts != null)
                    record.SaturdayEffort = ts.HoursWorked;

                recordDate = Convert.ToDateTime(FromDate);
                TimesheetRecords.Add(record);
            }
        }
    }
}
