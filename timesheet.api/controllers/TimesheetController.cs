﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.api.Models;
using timesheet.business;
using timesheet.model;
using timesheet.model.Helpers;

namespace timesheet.api.controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class TimesheetController : ControllerBase
    {
        private readonly ITimeSheetService _timeSheetEntryService;
        public TimesheetController(ITimeSheetService timeSheetEntryService)
        {
            _timeSheetEntryService = timeSheetEntryService;
        }

        [HttpGet("GetWeeklyTimesheet/{employeeId}/{week}")]
        public IActionResult GetWeeklyTimesheet(int employeeId, int week)
        {
            var days = week * 7;
            var weekStartDate = DateTime.Today.AddDays(days).StartOfWeek(DayOfWeek.Sunday);

            var model = new TimesheetModel();
            model.EmployeeId = employeeId;
            model.Week = week;
            model.FromDate = weekStartDate.ToString("dd/MM/yyyy");
            model.ToDate = weekStartDate.AddDays(6).ToString("dd/MM/yyyy");

            var timesheets = _timeSheetEntryService.GetWeeklyTimesheet(employeeId, week);
            model.MapToTimesheetModel(timesheets);

            return Ok(model);
        }

        [HttpPost]
        public IActionResult PostTimeSheetEntry([FromBody] TimesheetModel model)
        {
            var days = model.Week * 7;
            var weekStartDate = DateTime.Today.AddDays(days).StartOfWeek(DayOfWeek.Sunday);

            var timesheetList = new List<TimeSheetRecord>();
            foreach (var row in model.TimesheetRecords)
            {
                timesheetList.Add(new TimeSheetRecord()
                {
                    EmployeeId = model.EmployeeId,
                    TaskId = row.TaskId,
                    TimesheetDate = weekStartDate,
                    HoursWorked = row.SundayEffort
                });

                timesheetList.Add(new TimeSheetRecord()
                {
                    EmployeeId = model.EmployeeId,
                    TaskId = row.TaskId,
                    TimesheetDate = weekStartDate.AddDays(1),
                    HoursWorked = row.MondayEffort
                });

                timesheetList.Add(new TimeSheetRecord()
                {
                    EmployeeId = model.EmployeeId,
                    TaskId = row.TaskId,
                    TimesheetDate = weekStartDate.AddDays(2),
                    HoursWorked = row.TuesdayEffort
                });

                timesheetList.Add(new TimeSheetRecord()
                {
                    EmployeeId = model.EmployeeId,
                    TaskId = row.TaskId,
                    TimesheetDate = weekStartDate.AddDays(3),
                    HoursWorked = row.WednesdayEffort
                });
                timesheetList.Add(new TimeSheetRecord()
                {
                    EmployeeId = model.EmployeeId,
                    TaskId = row.TaskId,
                    TimesheetDate = weekStartDate.AddDays(4),
                    HoursWorked = row.ThursdayEffort
                });
                timesheetList.Add(new TimeSheetRecord()
                {
                    EmployeeId = model.EmployeeId,
                    TaskId = row.TaskId,
                    TimesheetDate = weekStartDate.AddDays(5),
                    HoursWorked = row.FridayEffort
                });
                timesheetList.Add(new TimeSheetRecord()
                {
                    EmployeeId = model.EmployeeId,
                    TaskId = row.TaskId,
                    TimesheetDate = weekStartDate.AddDays(6),
                    HoursWorked = row.SaturdayEffort
                });
            }

            _timeSheetEntryService.InsertTimesheet(timesheetList);

            return CreatedAtAction(nameof(PostTimeSheetEntry), null);
        }
    }
}