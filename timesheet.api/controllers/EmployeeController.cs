﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using timesheet.api.Models;
using timesheet.business;
using timesheet.model;
using timesheet.model.Helpers;

namespace timesheet.api.controllers
{
    [Route("api/v1/[Controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;
        public EmployeeController(IEmployeeService employeeService)
        {
            this._employeeService = employeeService;
        }

        [HttpGet("getall")]
        public IActionResult GetAll(string text)
        {
            var items = this._employeeService.GetEmployees();
            return Ok(items);
        }
        [HttpGet("GetEmployeeWeekEffortList")]
        public IActionResult GetEmployeeWeekEffortList()
        {
            List<EmployeeEffortItem> model = new List<EmployeeEffortItem>();
            var employees = _employeeService.GetEmployeeWithTimesheet();

            model = employees.Select(x => new EmployeeEffortItem
            {
                Id = x.Id,
                Code = x.Code,
                Name = x.Name,
                CurrentWeekEffort = GetCurrentWeekEffort(x.TimeSheetRecrods),
                AverageWeeklyEffort = GetAverageWeeklyEffort(x.TimeSheetRecrods)
            }).ToList();

            return Ok(model);
        }

        private double? GetCurrentWeekEffort(ICollection<TimeSheetRecord> timesheets)
        {
            if (timesheets == null || timesheets.Count == 0)
                return null;

            var timesheetList = timesheets.ToList();
            var currentWeekStartDate = DateTime.Today.StartOfWeek(DayOfWeek.Sunday);
            var currentWeekEndDate = currentWeekStartDate.AddDays(6);

            var totalHours = timesheetList.Where(x => x.TimesheetDate >= currentWeekStartDate && x.TimesheetDate <= currentWeekEndDate).Sum(x => x.HoursWorked);
            if (!totalHours.HasValue)
                return null;

            return Math.Round(totalHours.Value, 2);
        }

        private double? GetAverageWeeklyEffort(ICollection<TimeSheetRecord> timesheets)
        {
            if (timesheets == null || timesheets.Count == 0)
                return null;

            var timesheetList = timesheets.ToList();
            var currentWeekStartDate = DateTime.Today.StartOfWeek(DayOfWeek.Sunday);
            var currentWeekEndDate = currentWeekStartDate.AddDays(6);

            var totalHours = timesheetList.Sum(x => x.HoursWorked);
            var minDate = timesheetList.Min(x => x.TimesheetDate);
            var maxDate = timesheetList.Max(x => x.TimesheetDate);

            var totalDays = (maxDate - minDate).TotalDays;
            var totalWeeks = (totalDays + 1) / 7;

            if (!totalHours.HasValue)
                return null;

            return Math.Round(totalHours.Value / totalWeeks, 2);
        }
    }
}


