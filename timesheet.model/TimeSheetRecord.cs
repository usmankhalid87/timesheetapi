﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace timesheet.model
{
    public class TimeSheetRecord
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "Date")]
        public DateTime TimesheetDate { get; set; }

        public double? HoursWorked { get; set; }

        [ForeignKey("Task")]
        public int TaskId { get; set; }

        [ForeignKey("Employee")]
        public int EmployeeId { get; set; }

        public Task Task { get; set; }

        public Employee Employee { get; set; }
    }
}
