﻿using System;
using System.Collections.Generic;
using System.Linq;
using timesheet.data;
using timesheet.model;
using timesheet.model.Helpers;

namespace timesheet.business
{
    public class TimeSheetService : ITimeSheetService
    {
        private readonly TimesheetDb _context;

        public TimeSheetService(TimesheetDb context)
        {
            _context = context;

        }

        public IEnumerable<TimeSheetRecord> GetWeeklyTimesheet(int employeeId, int week)
        {
            var days = week * 7;
            var weekStartDate = DateTime.Today.AddDays(days).StartOfWeek(DayOfWeek.Sunday);
            var lastDayOfWeek = weekStartDate.AddDays(6);

            return _context.TimeSheetRecords.Where(x => x.EmployeeId == employeeId &&
                                                        x.TimesheetDate >= weekStartDate &&
                                                        x.TimesheetDate <= lastDayOfWeek)
                                                        .OrderBy(x => x.TimesheetDate)
                                                        .ToList();
        }

        public void InsertTimesheet(List<TimeSheetRecord> timesheetRecords)
        {
            var employeeId = timesheetRecords.Select(x => x.EmployeeId).Distinct().SingleOrDefault();
            var taskIds = timesheetRecords.Select(x => x.TaskId).Distinct().ToList();
            var weekStartDate = timesheetRecords.Select(x => x.TimesheetDate).Min();
            var weekEndDate = timesheetRecords.Select(x => x.TimesheetDate).Max();

            var existingRecords = _context.TimeSheetRecords.Where(x => x.EmployeeId == employeeId &&
                                                                        taskIds.Contains(x.TaskId) &&
                                                                        x.TimesheetDate >= weekStartDate &&
                                                                        x.TimesheetDate <= weekEndDate).ToList();


            foreach (var timesheetRecord in timesheetRecords)
            {
                var record = existingRecords.SingleOrDefault(x => x.TaskId == timesheetRecord.TaskId &&
                                                     x.TimesheetDate == timesheetRecord.TimesheetDate);

                if (record != null)
                    record.HoursWorked = timesheetRecord.HoursWorked;
                else
                    _context.TimeSheetRecords.Add(timesheetRecord);
            }

            _context.SaveChanges();
        }
    }
}
