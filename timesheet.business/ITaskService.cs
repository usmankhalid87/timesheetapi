﻿using System;
using System.Collections.Generic;
using System.Text;
using timesheet.model;

namespace timesheet.business
{
    public interface ITaskService
    {
        IEnumerable<Task> GetAll();
    }
}
