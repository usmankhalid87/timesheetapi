﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class TaskService : ITaskService
    {
        private readonly TimesheetDb _context;

        public TaskService(TimesheetDb context)
        {
            _context = context;

        }
        public IEnumerable<Task> GetAll()
        {
            return this._context.Tasks.ToList();
        }
    }
}
