﻿using System;
using System.Collections.Generic;
using System.Text;
using timesheet.model;

namespace timesheet.business
{
    public interface ITimeSheetService
    {
        IEnumerable<TimeSheetRecord> GetWeeklyTimesheet(int employeeId, int week);

        void InsertTimesheet(List<TimeSheetRecord> timesheetRecords);
    }
}
