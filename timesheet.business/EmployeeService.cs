﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeService : IEmployeeService
    {
        public TimesheetDb db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IEnumerable<Employee> GetEmployees()
        {
            return this.db.Employees.ToList();
        }

        public IEnumerable<Employee> GetEmployeeWithTimesheet()
        {
            return  db.Employees.Include(x => x.TimeSheetRecrods).ToList();
        }
    }
}
