﻿using System;
using System.Collections.Generic;
using System.Text;
using timesheet.model;

namespace timesheet.business
{
    public interface IEmployeeService
    {
        IEnumerable<Employee> GetEmployees();

        IEnumerable<Employee> GetEmployeeWithTimesheet();
    }
}
